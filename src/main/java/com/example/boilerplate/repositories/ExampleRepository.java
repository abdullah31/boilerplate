package com.example.boilerplate.repositories;

import com.example.boilerplate.repositories.entities.Example;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ExampleRepository extends JpaRepository<Example,Integer> {

}
