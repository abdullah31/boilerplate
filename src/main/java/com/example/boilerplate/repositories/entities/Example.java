package com.example.boilerplate.repositories.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table("example")
@AllArgsConstructor
@NoArgsConstructor
public class Example {
    private int id;
    private String name;
}
