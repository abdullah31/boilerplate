# PROJECT NAME
## Description
this is boilerplate of MTT spring boot frame work implementation 
written in java spring boot

## Prerequisites
1. java version
2. database version
3. spring boot version
4. maven or gradle version
5. and other setup configuration

## Installation
### how to build
_example_

```mvn install```
### how to run
_example_

```mvn run```
### how to test
_example_

```mvn test```

## Contributors
1. Dolla abdullah@maxxitani.id
2. Riszkhy riszkhy@maxxitani.id 
